
/*
    Name:       i2cEEPROMClean.ino
    Created:	04/07/2018 23:39:57
    Author:     COLLO-HP\Collo
*/

#define EEPROM_ADDRESS	0X50
#define I2C_TIMEOUT		1000
#define MAX_PAGE_SIZE	64

#define MSG_START		0X1B
#define TOKEN			0X0E

#define CMD_LOAD_ADDRESS                    0x06
#define CMD_ENTER_PROGMODE_ISP              0x10
#define CMD_LEAVE_PROGMODE_ISP              0x11
#define CMD_PROGRAM_FLASH_ISP               0x13

#include "I2C.h"
#include <EEPROM.h>
#include "CRC32.h"


void init_i2c_eeprom();
uint8_t poll();
uint8_t read_i2c_eeprom(uint8_t *);
uint8_t set_i2c_eeprom_read_pointer(uint16_t, uint8_t*);
uint8_t write_to_i2c_eeprom(uint16_t, uint8_t*);
uint8_t write_firmware_to_i2c_eeprom();
uint8_t get_hex_4(uint8_t);
uint8_t stk_start_new_message(uint8_t, uint8_t);
uint8_t i2c_send_address(uint16_t);


uint8_t stkSequenceNumber = 1;
uint8_t flashState = CMD_LOAD_ADDRESS;

#define HEX_START_CHAR 0x3A

#define STATE_START_LINE 1
#define STATE_GET_DATA_COUNT 2
#define STATE_GET_ADDRESS 3
#define STATE_GET_RECORD_TYPE 4
#define STATE_READ_DATA 5
#define STATE_GET_CHECKSUM 6

// const char server[] = "vsh.pp.ua";
// const int port = 80;
// 
// const char resource[] = "/TinyGSM/test_1k.bin";
// 
// 
// void download_file_to_eeprom(TinyGsmClient, const char*);
// 
// void download_file_to_eeprom(TinyGsmClient _client, const char* _address)
// {
// 	
// }


void init_i2c_eeprom()
{
	Serial.println("INITIALISING");
	I2c.begin();
	I2c.timeOut(I2C_TIMEOUT);
	I2c.pullup(true);
	I2c.setSpeed(1);	
}

uint8_t get_hex_4(uint8_t i)
{
	if      (i >= 0x61 && i <= 0x66) return i - 0x61 + 10;
	else if (i >= 0x41 && i <= 0x46) return i - 0x41 + 10;
	else if (i >= 0x30 && i <= 0x39) return i - 0x30;
	else return 0;

}

uint8_t stk_start_new_message(uint16_t messageSize, uint8_t *_checksum)
{
	Serial.println("new message");
	uint8_t returnStatus = 0;
	returnStatus = I2c.sendByte(MSG_START);
	*_checksum ^= MSG_START;
	if(returnStatus){
		return returnStatus;
	}
	
	returnStatus = I2c.sendByte(stkSequenceNumber);
	*_checksum ^= stkSequenceNumber;
	if(returnStatus){
		return returnStatus;
	}
	stkSequenceNumber++;
	
	returnStatus = I2c.sendByte(messageSize&0xFF);
	*_checksum ^= (messageSize & 0xFF);
	if(returnStatus){
		return returnStatus;
	}
	
	returnStatus = I2c.sendByte((messageSize>>8)&0xFF);
	*_checksum ^= (messageSize>>8) & 0xFF;
	if(returnStatus){
		return returnStatus;
	}
	
	returnStatus = I2c.sendByte(TOKEN);
	*_checksum ^= TOKEN;
	if(returnStatus){
		return returnStatus;
	}
	
	return returnStatus;
}

uint8_t poll()
{
	Serial.println("[POLL]");
	uint8_t returnStatus = I2c.start();
	if(!returnStatus){
		returnStatus = I2c.sendAddress(SLA_W(EEPROM_ADDRESS));
	}
	else{
		return returnStatus;
	}
	
	if(!returnStatus){
		return returnStatus; //SUCCESS!!
	}
	else{
		return returnStatus;
	}	
}

uint8_t i2c_send_address(uint16_t a)
{
	Serial.println("\nSending Address");
	Serial.println(a);
	Serial.println("");
	uint8_t returnStatus = 0;
	returnStatus = I2c.sendByte((a>>8)&0xFF);
	if(returnStatus){
		return returnStatus;
	}
	returnStatus = I2c.sendByte(a&0xFF);
	if(returnStatus){
		return returnStatus;
	}
	return returnStatus;
}

uint8_t read_i2c_eeprom(uint8_t *_buffer)
{
		uint8_t returnStatus = 0;
		returnStatus = I2c.start();
		if(returnStatus){
			return returnStatus;
		}
		
		returnStatus = I2c.sendAddress(SLA_R(EEPROM_ADDRESS));
		if(returnStatus){
			return returnStatus;
		}
		
		returnStatus = I2c.receiveByte(1, _buffer);
		if(returnStatus){
			return returnStatus;
		}
		
		return returnStatus; //SUCCESS!!	
}

uint8_t set_i2c_eeprom_read_pointer(uint16_t _address)
{
	//todo add byte count
	uint8_t returnStatus = 0;
	_address--;
	
	returnStatus = I2c.start();
	if(returnStatus){
		return returnStatus;
	}
	
	returnStatus = I2c.sendAddress(SLA_W(EEPROM_ADDRESS));
	if(returnStatus){
		return returnStatus;
	}
	
	returnStatus = I2c.sendByte((_address>>8)&0xFF);
	if(returnStatus){
		return returnStatus;
	}
	
	returnStatus = I2c.sendByte(_address&0xFF);
	if(returnStatus){
		return returnStatus;
	}
	
	returnStatus = I2c.start();
	if(returnStatus){
		return returnStatus;
	}
	
	returnStatus = I2c.sendAddress(SLA_R(EEPROM_ADDRESS));
	if(returnStatus){
		return returnStatus;
	}
	uint8_t *_buffer;
	returnStatus = I2c.receiveByte(1, _buffer);
	_buffer++;
	if(returnStatus){
		return returnStatus;
	}
	
	returnStatus = I2c.stop();
	if(returnStatus){
		return returnStatus;
	}
	return returnStatus;
}

uint8_t write_to_i2c_eeprom(uint16_t _address, uint8_t *_data)
{
	uint8_t byteCount = 0;
	uint8_t returnStatus = 0;
	uint8_t _addressHB;
	uint8_t _addressLB;
	
	while(*_data){
		if(byteCount == MAX_PAGE_SIZE){
			byteCount = 0;
			_address += MAX_PAGE_SIZE;
			returnStatus = I2c.stop();
			if(returnStatus){
				return returnStatus;
			}
		}
		if(byteCount == 0){
			returnStatus = poll();
			int timeout = 0;
			while(returnStatus){
				returnStatus = poll();//todo timeout
				_delay_ms(5);
				timeout++;
				if(timeout > 10){
					return 1;
				}
			}
			
			_addressLB = _address & 0xFF;
			_addressHB = (_address>>8) & 0xFF;
			
			returnStatus = I2c.sendByte(_addressHB);
			if(returnStatus){
				return returnStatus;
			}
	
			returnStatus = I2c.sendByte(_addressLB);
			if(returnStatus){
				return returnStatus;
			}
		}
		
		returnStatus = I2c.sendByte(*_data);
		if(returnStatus){
			return returnStatus;
		}
		_data++;
		byteCount++;
	}
	
	returnStatus = I2c.stop();
	if(returnStatus){
		return returnStatus;
	}
	return returnStatus;
}

uint16_t globalAddress = 0;

uint8_t write_firmware_to_i2c_eeprom(uint16_t sAddress, uint8_t *_data)
{
	Serial.println("WRITING TO EEPROM");
	uint8_t i2cByteCount = 0;
	uint8_t progAddressLoaded = 0;
	uint16_t _address = sAddress;
	uint8_t stkChecksum = 0;
	uint8_t *f_data;
	uint8_t checksum = 0;
	uint8_t ready_to_write = 0;
	uint8_t parseState = 0;
	uint8_t _byteCount = 0;
	uint8_t _recordType = 0;
	uint8_t addressMSB = 0;
	uint8_t addressLSB = 0;
	flashState = 0;
	
	while(*_data){
		if((*_data == 0x3A) && (parseState < 1)){
			Serial.println(*_data, HEX);
			Serial.println("LINE START");
			parseState = 1;
			ready_to_write = 0;
			
			checksum = 0;
			_byteCount = 0;
			_recordType = 0;
			addressMSB = 0;
			addressLSB = 0;
			
			_data++;
		}
		
		if(parseState == 1){ //get byte count
			_byteCount |= ((get_hex_4(*_data)<<4));
			_data++;
			_byteCount |= (get_hex_4(*_data));
			_data++;
			
			parseState = 2;
		}
		
		
		if(parseState == 2){ // get address
			addressLSB |= (get_hex_4(*_data)<<12);
			_data++;
			addressLSB |= (get_hex_4(*_data)<<8);
			_data++;
			addressMSB |= (get_hex_4(*_data)<<4);
			_data++;
			addressMSB |= (get_hex_4(*_data));
			_data++;
			
			parseState = 3;
		}
			
		if(parseState == 3){//get record type
			_recordType |= (get_hex_4(*_data)<<4);
			_data++;
			_recordType |= (get_hex_4(*_data));
			_data++;
			
			if(_recordType == 0x00){
				parseState = 4;
			}
			else if(_recordType == 0x01){
				Serial.println(_recordType);
				ready_to_write = 1;
				flashState = CMD_LEAVE_PROGMODE_ISP;
			}
		}
		
		if(parseState == 4){//get data
			for(uint8_t i=0; i<_byteCount; ++i){
				f_data[i] |= (get_hex_4(*_data)<<4);
				_data++;
				f_data[i] |= (get_hex_4(*_data));
				_data++;
			}
			parseState = 5;
		}
		
		if(parseState == 5){
			checksum |= (get_hex_4(*_data)<<4);
			_data++;
			checksum |= (get_hex_4(*_data));
			_data++;
			
			parseState = 6;
			
			flashState = CMD_LOAD_ADDRESS;
		}
		
		if(parseState == 6){
			ready_to_write = 1;
		}

		if(ready_to_write){
			
			uint8_t returnStatus = 0;
			
			unsigned long timeout = millis();
			returnStatus = poll();
			while(returnStatus){
				returnStatus = poll();
				
				if((millis() - timeout) > 10000L){
					Serial.println("Unable to Communicate With EEPROM");
					return returnStatus;
				}
			}
			
			returnStatus = i2c_send_address(_address + i2cByteCount);
			if(returnStatus){
				Serial.println("UNABLE TO SEND ADDRESS");
				return returnStatus;
			}
			
			switch(flashState){
				case CMD_LOAD_ADDRESS:{
					Serial.println("LOAD ADDRESS");
					stkChecksum = 0;
					
					uint16_t messageSize = 5;
					
					returnStatus = stk_start_new_message(messageSize, &stkChecksum);
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount += 5;
					
					returnStatus = I2c.sendByte(CMD_LOAD_ADDRESS);
					stkChecksum ^= CMD_LOAD_ADDRESS;
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount++;
					
					returnStatus = I2c.sendByte(addressMSB);
					stkChecksum ^= (addressMSB);
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount++;
					
					returnStatus = I2c.sendByte(addressLSB);
					stkChecksum ^= addressLSB;
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount++;
					
					returnStatus = I2c.sendByte(0x00);
					stkChecksum ^= (0x00) & 0xFF;
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount++;
					returnStatus = I2c.sendByte(0x10);
					stkChecksum ^= (0x10) & 0xFF;
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount++;
					
					returnStatus = I2c.sendByte(stkChecksum);
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount++;
					
					returnStatus = I2c.stop();
					if(returnStatus){
						return returnStatus;
					}
					
					flashState = CMD_ENTER_PROGMODE_ISP;
					Serial.println("address");
					Serial.println(i2cByteCount);
					
					break;
				}
				
				case CMD_ENTER_PROGMODE_ISP:{
					Serial.println("CMD ENTER PROG MODE");
					stkChecksum = 0;
					
					uint16_t messageSize = 1;
					
					returnStatus = stk_start_new_message(messageSize, &stkChecksum);
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount += 5;
					
					returnStatus = I2c.sendByte(CMD_ENTER_PROGMODE_ISP);
					stkChecksum ^= CMD_ENTER_PROGMODE_ISP;
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount++;
					
					returnStatus = I2c.sendByte(stkChecksum);
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount++;
					
					Serial.println("count");
					Serial.println(i2cByteCount);
					returnStatus = I2c.stop();
					if(returnStatus){
						return returnStatus;
					}
					
					flashState = CMD_PROGRAM_FLASH_ISP;
					break;
				}
				
				case CMD_PROGRAM_FLASH_ISP:{
					Serial.println("CMD PROGRAM FLASH");
					stkChecksum = 0;
					
					uint16_t messageSize = _byteCount + 3;
					
					returnStatus = stk_start_new_message(messageSize, &stkChecksum);
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount += 5;
					
					returnStatus = I2c.sendByte(CMD_PROGRAM_FLASH_ISP);
					stkChecksum ^= CMD_PROGRAM_FLASH_ISP;
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount++;
					
					returnStatus = I2c.sendByte(_byteCount);
					stkChecksum ^= (_byteCount);
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount++;
					returnStatus = I2c.sendByte(0x00);
					stkChecksum ^= 0x00;
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount++;
					
					for(uint8_t i=0; i<(int)_byteCount; ++i){
						returnStatus = I2c.sendByte(f_data[i]);
						stkChecksum ^= f_data[i];
						if(returnStatus){
							return returnStatus;
						}
						i2cByteCount++;
					}
					
					returnStatus = I2c.sendByte(stkChecksum);
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount++;
					
					returnStatus = I2c.stop();
					if(returnStatus){
						return returnStatus;
					}
					
					if(parseState == 7){
						parseState = 0;
						flashState = CMD_LEAVE_PROGMODE_ISP;
					}
					else{
						flashState = 0;
						parseState = 0;
					}
					Serial.println(i2cByteCount);
					break;
				}
				
				case CMD_LEAVE_PROGMODE_ISP:{
					Serial.println("CMD LEAVE PROG MODE");
					stkChecksum = 0;
					
					uint16_t messageSize = 1;
					
					returnStatus = stk_start_new_message(messageSize, &stkChecksum);
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount += 5;
					
					returnStatus = I2c.sendByte(CMD_LEAVE_PROGMODE_ISP);
					stkChecksum ^= CMD_LEAVE_PROGMODE_ISP;
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount++;
					
					returnStatus = I2c.sendByte(stkChecksum);
					if(returnStatus){
						return returnStatus;
					}
					i2cByteCount++;
					
					returnStatus = I2c.stop();
					if(returnStatus){
						return returnStatus;
					}
					return 0;
				}
				case 0:{
					_data++;
				}
			}
			
		}
		
	}
	
	Serial.println("DATA:");
	Serial.println(*_data);
	globalAddress = _address;
	Serial.println(globalAddress);
	return 0;
}

char *hexData = ":1000A000C8C00000C6C00000C4C00000C2C000003C\
:00000001FF";


void setup()
{
	Serial.begin(9600);
	delay(500);
	init_i2c_eeprom();
	
	uint8_t state;
	state = write_firmware_to_i2c_eeprom(1, hexData);
	while(state){
		Serial.println("WRITE FAIL");
	}
	state = I2c.stop();
	_delay_ms(25);
	Serial.println("WRITE SUCCESS");
	Serial.println("READING ENTIRE CHIP");
	set_i2c_eeprom_read_pointer(1);
// 	
// 	// 		uint8_t *buffer;
// 	//
// 	// 		byteCount = 0;
// 	//
// 	// 		while(byteCount < 4){
// 	// 			state = I2c.receiveByte(1, buffer);
// 	// 			while(state){
// 	// 				Serial.println("FAIL");
// 	// 				state = I2c.receiveByte(1, buffer);
// 	// 			}
// 	// 			Serial.println(*buffer, HEX);
// 	// 			buffer++;
// 	// 			byteCount++;
// 	// 		}
// 	EEPROM.write(0, 1);
}

uint8_t state = 0;
uint8_t *buffer = 0;
void loop()
{
// 		Serial.println("READING DATA");
//
// 		state = I2c.start();
// 		while(state){
// 			Serial.println("Start fail");
// 		}
// 		Serial.println("SENDING ADRRES");
// 		state = I2c.sendAddress(SLA_R(EEPROM_ADDR));
// 		while(state){
// 			Serial.println("Unable communicate");
// 			state = I2c.sendAddress(SLA_R(EEPROM_ADDR));
// 		}
// 		Serial.println("READING ENTIRE CHIP");
//
// 		uint8_t *buffer = NULL;
//
// 		while(byteCount < 256000){
// 			state = I2c.receiveByte(1, buffer);
// 			while(state){
// 				Serial.println("FAIL");
// 				state = I2c.receiveByte(1, buffer);
// 			}
// 			Serial.println(*buffer, HEX);
// 			buffer++;
// 			byteCount++;
// 		}
// 			state = I2c.receiveByte(1, buffer);
// 			while(state){
// 				Serial.println("FAIL");
// 				state = I2c.receiveByte(1, buffer);
// 			}

state = read_i2c_eeprom(buffer);
if(!state){
	Serial.println(*buffer, HEX);
}
}
