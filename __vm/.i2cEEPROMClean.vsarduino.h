/* 
	Editor: https://www.visualmicro.com/
			visual micro and the arduino ide ignore this code during compilation. this code is automatically maintained by visualmicro, manual changes to this file will be overwritten
			the contents of the Visual Micro sketch sub folder can be deleted prior to publishing a project
			all non-arduino files created by visual micro and all visual studio project or solution files can be freely deleted and are not required to compile a sketch (do not delete your own code!).
			note: debugger breakpoints are stored in '.sln' or '.asln' files, knowledge of last uploaded breakpoints is stored in the upload.vmps.xml file. Both files are required to continue a previous debug session without needing to compile and upload again
	
	Hardware: Arduino/Genuino Mega w/ ATmega2560 (Mega 2560), Platform=avr, Package=arduino
*/

#define __AVR_ATmega2560__
#define ARDUINO 10803
#define ARDUINO_MAIN
#define F_CPU 16000000L
#define __AVR__
#define F_CPU 16000000L
#define ARDUINO 10803
#define ARDUINO_AVR_MEGA2560
#define ARDUINO_ARCH_AVR
uint8_t get_hex_4(uint8_t i);
uint8_t stk_start_new_message(uint16_t messageSize, uint8_t *_checksum);
uint8_t i2c_send_address(uint16_t a);
uint8_t read_i2c_eeprom(uint8_t *_buffer);
uint8_t set_i2c_eeprom_read_pointer(uint16_t _address);
uint8_t write_to_i2c_eeprom(uint16_t _address, uint8_t *_data);
uint8_t write_firmware_to_i2c_eeprom(uint16_t sAddress, uint8_t *_data);
//
//

#include "pins_arduino.h" 
#include "arduino.h"
#include "i2cEEPROMClean.ino"
